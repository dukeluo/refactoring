package com.twu.refactoring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Direction {
    private final char direction;

    public Direction(char direction) {
        this.direction = direction;
    }

    public Direction turnRight() {
        List<Character> directions = Arrays.asList('N', 'E', 'S', 'W');
        int index = directions.indexOf(this.direction);

        if (index == -1) {
            throw new IllegalArgumentException();
        }
        return new Direction(directions.get((index + 1) % 4));
    }

    public Direction turnLeft() {
        List<Character> directions = Arrays.asList('N', 'E', 'S', 'W');
        int index = directions.indexOf(this.direction);

        if (index == -1) {
            throw new IllegalArgumentException();
        }
        return new Direction(directions.get((index + 3) % 4));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Direction direction1 = (Direction) o;

        if (direction != direction1.direction) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) direction;
    }

    @Override
    public String toString() {
        return "Direction{direction=" + direction + '}';
    }
}
