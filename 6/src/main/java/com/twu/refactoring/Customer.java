package com.twu.refactoring;

import java.util.ArrayList;

public class Customer {

	private String name;
	private ArrayList<Rental> rentals = new ArrayList<Rental>();

	public Customer(String name) {
		this.name = name;
	}

	public void addRental(Rental arg) {
		rentals.add(arg);
	}

	public String getName() {
		return name;
	}

	public String statement() {
		double totalAmount = rentals.stream().map(Rental::getAmount).mapToDouble(Double::doubleValue).sum();;
		int frequentRenterPoints = rentals.stream().map(Rental::getFrequentRenterPoints).mapToInt(Integer::intValue).sum();
        String result = "Rental Record for " + getName() + "\n";

        for (Rental rental : rentals) {
            result += "\t" + rental.getMovie().getTitle() + "\t"
                    + String.valueOf(rental.getAmount()) + "\n";
        }
		result += "Amount owed is " + String.valueOf(totalAmount) + "\n";
		result += "You earned " + String.valueOf(frequentRenterPoints)
				+ " frequent renter points";
		return result;
	}
}
