package com.twu.refactoring;

/**
 * OrderReceipt prints the details of order including customer name, address, description, quantity,
 * price and amount. It also calculates the sales tax @ 10% and prints as part
 * of order. It computes the total order amount (amount of individual lineItems +
 * total sales tax) and prints it.
 * 
 */
public class OrderReceipt {
    private Order order;

    public OrderReceipt(Order order) {
        this.order = order;
	}

	public String printReceipt() {
		StringBuilder output = new StringBuilder();

		output.append("======Printing Orders======\n");
        output.append(order.getCustomerName());
        output.append(order.getCustomerAddress());
		for (LineItem lineItem : order.getLineItems()) {
			output.append(lineItem.getDescription());
			output.append('\t');
			output.append(lineItem.getPrice());
			output.append('\t');
			output.append(lineItem.getQuantity());
			output.append('\t');
			output.append(lineItem.totalAmount());
			output.append('\n');
		}
		output.append("Sales Tax").append('\t').append(getTatalSalesTax());
		output.append("Total Amount").append('\t').append(getTotalAmount());
		return output.toString();
	}

	private double getTotalAmount() {
        double totalAmount = 0d;

        for (LineItem lineItem : order.getLineItems()) {
            totalAmount += lineItem.totalAmount();
        }
        totalAmount += getTatalSalesTax();
        return totalAmount;
    }

    private double getSalesTax(LineItem lineItem) {
        return lineItem.totalAmount() * .10;
    }

    private double getTatalSalesTax() {
        double totalSalesTax = 0d;

        for (LineItem lineItem : order.getLineItems()) {
            totalSalesTax += getSalesTax(lineItem);
        }
        return totalSalesTax;
    }
}